#
#  Be sure to run `pod spec lint Auth-IA.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  s.name         = "Auth-IA"
  s.version      = "0.3.2"
  s.summary      = "AUth IA for for All of IA Projects"
  s.homepage = "https://bitbucket.org/m-arslan-IA/auth-ia/"


  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Licensing your code is important. See http://choosealicense.com for more info.
  #  CocoaPods will detect a license file if there is a named LICENSE*
  #  Popular ones are 'MIT', 'BSD' and 'Apache License, Version 2.0'.
  #

  s.license          = { :type => 'MIT', :file => 'LICENSE' }


  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the authors of the library, with email addresses. Email addresses
  #  of the authors are extracted from the SCM log. E.g. $ git log. CocoaPods also
  #  accepts just a name if you'd rather not provide an email address.
  #
  #  Specify a social_media_url where others can refer to, for example a twitter
  #  profile URL.
  #

  s.author             = { "Muhammad Arslan Asim" => "m.arslan@ia.gov.ae" }
  # Or just: s.author    = "Muhammad Arslan Asim"
  # s.authors            = { "Muhammad Arslan Asim" => "m.arslan@ia.gov.ae" }
  # s.social_media_url   = "http://twitter.com/Muhammad Arslan Asim"

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If this Pod runs only on iOS or OS X, then specify the platform and
  #  the deployment target. You can optionally include the target after the platform.
  #

    s.platform     = :ios
    s.ios.deployment_target = '10.3'

  #  When using multiple platforms
  # s.ios.deployment_target = "5.0"
  # s.osx.deployment_target = "10.7"
  # s.watchos.deployment_target = "2.0"
  # s.tvos.deployment_target = "9.0"


  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the location from where the source should be retrieved.
  #  Supports git, hg, bzr, svn and HTTP.
  #

  s.source       = { :git => "https://m-arslan-IA@bitbucket.org/m-arslan-IA/auth-ia.git", :tag => "#{s.version}" }



    # 7
    s.framework = "UIKit"
    s.dependency 'Alamofire', '~> 4.8'
    s.dependency 'ObjectMapper', '~> 3.4'
    s.dependency 'AlamofireObjectMapper', '~> 5.2'
    s.dependency 'M13Checkbox', '~> 3.3'
    s.dependency 'SwiftMessages', '~> 6.0'
    s.dependency 'CryptoSwift', '~> 0.13'
    s.dependency 'TPKeyboardAvoiding' , '~> 1.3'
    s.dependency 'ActionSheetPicker-3.0', '~> 2.3'
    s.dependency 'JWT', '~> 2.2'
    s.dependency 'SVProgressHUD', '~> 2.2'
    s.dependency 'RealmSwift', '~> 3.13'
    s.dependency 'IAFloatingMaskedTextField'
    s.dependency 'IAExtenstions'
    # 8
    s.source_files = "Auth-IA/**/*.{swift}"

    # 9
    s.resources = "Auth-IA/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"
    s.resource_bundle = { "Auth-IA" => ["Auth-IA/**/*.lproj/*.strings"] }
    # 10
    s.swift_version = "4.2"
    s.requires_arc = true

end
