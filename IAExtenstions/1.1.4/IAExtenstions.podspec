#
# Be sure to run `pod lib lint IAExtenstions.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'IAExtenstions'
  s.version          = '1.1.4'
  s.summary          = 'All the extentions to be used in Project'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.homepage         = 'https://bitbucket.org/m-arslan-IA/insuranceauthorityextensions'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Muhammad Arslan Asim' => 'm.arslan@ia.gov.ae' }
  s.source           = { :git => 'https://m-arslan-IA@bitbucket.org/m-arslan-IA/insuranceauthorityextensions.git', :tag => s.version.to_s }
  s.swift_version    = '4.2'
  

  s.ios.deployment_target = '10.0'

  s.source_files = 'IAExtenstions/Classes/**/*'
  
  # s.resource_bundles = {
  #   'IAExtenstions' => ['IAExtenstions/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
