#
# Be sure to run `pod lib lint FE-IA.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'FE-IA'
  s.version          = '0.2.0'
  s.summary          = 'FE-IA is the pod will have company cancellation module from Followup & Enforcement.'
  s.description      = 'FE-IA will have compnay cancellation request, cancellation details will be added in future updates.'
  s.homepage         = 'https://syedismailahamed@bitbucket.org/syedismailahamed/fe-ia.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Syed Ismail Ahamed' => 'ismailahamedsyed@gmail.com' }
  s.source           = { :git => 'https://syedismailahamed@bitbucket.org/syedismailahamed/fe-ia.git', :tag => s.version.to_s }
  s.ios.deployment_target = '10.3'
  s.swift_versions   = ['4.2']
  s.source_files = 'FE-IA/Classes/**/*'
  s.resources = "FE-IA/Assets/**/*.{png,jpeg,jpg,storyboard,xib,xcassets,strings,json}"

 # pod 'ChameleonFramework/Swift', :git => 'https://github.com/luckychris/Chameleon.git', :inhibit_warnings => true
 s.framework = "UIKit"
  s.dependency 'RxSwift'
  s.dependency 'RxCocoa'
  s.dependency 'ObjectMapper', '~> 3.4'
  s.dependency 'Alamofire'
  s.dependency 'AlamofireObjectMapper', '~> 5.2'
  s.dependency 'CryptoSwift', '~> 1.1.3'
  s.dependency 'IA-CustomComponents', '~> 2.0.6'
  s.dependency 'RSLoadingView', '~> 1.1'
  s.dependency 'SKPhotoBrowser', '~>6.0.0'
  s.dependency 'SwiftMessages'
  s.dependency "PromiseKit"
  s.dependency 'RealmSwift', '~>4.1.0'
  s.dependency 'Tabman', '~>2.1.2'



 
  # s.resource_bundles = {
  #   'FE-IA' => ['FE-IA/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
