Pod::Spec.new do |spec|
  spec.name         = "IA-Settings"
  spec.version      = "1.1"
  spec.summary      = "this will be added later"
  spec.homepage = "https://bitbucket.org/iauae/ia-settings/"

  spec.license          = { :type => 'MIT', :file => 'LICENSE' }
  spec.author             = { "Muhammad Arslan Asim" => "m.arslan@ia.gov.ae" }


    spec.platform     = :ios, "10.3"




  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  spec.source       = { :git => "https://iauae@bitbucket.org/iauae/ia-settings.git", :tag => "#{spec.version}" }


  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
    spec.source_files = "IA-Settings/**/*.{swift}"
    spec.resources = "IA-Settings/**/*.{png,jpeg,jpg,storyboard,xib,xcassets,strings}"
    spec.swift_version = "4.2"
    spec.requires_arc = true
    spec.dependency 'IAExtenstions'
end
