#
#  Be sure to run `s.dependency spec lint LR_Module_IOS.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  s.name         = "LR_Module_IOS"
  s.version      = "2.5"
  s.summary      = "LR Module for for All of IA Projects"
  s.homepage     = "https://bitbucket.org/iauae/lr_module_ios/"


  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Licensing your code is important. See http://choosealicense.com for more info.
  #  CocoaPods will detect a license file if there is a named LICENSE*
  #  Popular ones are 'MIT', 'BSD' and 'Apache License, Version 2.0'.
  #

  s.license          = { :type => 'MIT', :file => 'LICENSE' }


  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the authors of the library, with email addresses. Email addresses
  #  of the authors are extracted from the SCM log. E.g. $ git log. CocoaPods also
  #  accepts just a name if you'd rather not provide an email address.
  #
  #  Specify a social_media_url where others can refer to, for example a twitter
  #  profile URL.
  #

  s.author             = { "Muhammad Yousuf Saif" => "m.saif@ia.gov.ae" }
  # Or just: s.author    = "Muhammad Yousuf Saif"
  # s.authors            = { "Muhammad Yousuf Saif" => "m.saif@ia.gov.ae" }

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If this s.dependency runs only on iOS or OS X, then specify the platform and
  #  the deployment target. You can optionally include the target after the platform.
  #

    s.platform     = :ios
    s.ios.deployment_target = '10.3'

  #  When using multiple platforms
  # s.ios.deployment_target = "5.0"
  # s.osx.deployment_target = "10.7"
  # s.watchos.deployment_target = "2.0"
  # s.tvos.deployment_target = "9.0"


  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the location from where the source should be retrieved.
  #  Supports git, hg, bzr, svn and HTTP.
  #

  s.source = { :git => "https://iauae@bitbucket.org/iauae/lr_module_ios.git", :tag => "#{s.version}" }

    # 7
    s.framework = "UIKit"
    s.dependency 'RxSwift'
    s.dependency 'RxCocoa'
    s.dependency 'RxDataSources'
    s.dependency 'AlamofireImage'
    s.dependency 'PromiseKit'
    s.dependency 'Tabman'
    s.dependency 'Charts'
    s.dependency 'LGSideMenuController'
    s.dependency 'lottie-ios'
    s.dependency 'Auth-IA', '~> 2.3'
    s.dependency 'IA-Settings', '~> 1.1'
    s.dependency 'IA-CustomComponents', '~> 2.0'
    
    # 8
    s.source_files = "LR_Module_IOS/**/*.{swift}"

    # 9
    s.resources = "LR_Module_IOS/**/*.{pdf,png,jpeg,jpg,storyboard,xib,xcassets}"
    # s.resource_bundle = { "LR_Module_IOS" => ["LR_Module_IOS/**/*.lproj/*.strings"] }
    # 10
    s.swift_version = "4.2"
    s.requires_arc = true

end

