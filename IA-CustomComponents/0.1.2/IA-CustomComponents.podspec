
Pod::Spec.new do |s|
    s.name         = "IA-CustomComponents"
    s.version      = "0.1.2"
    s.summary      = "Form compnonets to be used in IA Projects."
    s.homepage = "https://bitbucket.org/m-arslan-IA/ia-customcomponents"
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author             = { "Muhammad Arslan Asim" => "m.arslan@ia.gov.ae" }
    s.platform     = :ios
    s.ios.deployment_target = '10.3'
    s.source       = { :git => "https://m-arslan-IA@bitbucket.org/m-arslan-IA/ia-customcomponents", :tag => "#{s.version}" }
    s.swift_version = "4.2"
    s.requires_arc = true
    s.source_files = "IA-CustomComponents/**/*.{swift}"

    s.dependency 'IAFloatingMaskedTextField'
    s.dependency 'IAExtenstions'
    s.dependency 'SnapKit', '~> 4.2'
    s.dependency 'MGSwipeTableCell', '~> 1.6'
    s.dependency 'TPKeyboardAvoiding', '~> 1.3'
    s.dependency 'RichEditorView', '~> 5.0'
    s.resources = "IA-CustomComponents/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"
end
