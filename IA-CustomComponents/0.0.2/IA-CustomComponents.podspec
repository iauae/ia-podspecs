#
#  Be sure to run `pod spec lint IA-CustomComponents.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|
    s.name         = "IA-CustomComponents"
    s.version      = "0.0.2"
    s.summary      = "Form compnonets to be used in IA Projects."
    s.homepage = "https://bitbucket.org/m-arslan-IA/ia-customcomponents/"
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author             = { "Muhammad Arslan Asim" => "m.arslan@ia.gov.ae" }
    s.platform     = :ios
    s.ios.deployment_target = '10.3'
    s.source       = { :git => "https://m-arslan-IA@bitbucket.org/m-arslan-IA/ia-customcomponents", :tag => "#{s.version}" }
    s.swift_version = "4.2"
    s.requires_arc = true
    s.source_files = "IA-CustomComponents/**/*.{swift}"
end
